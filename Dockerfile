FROM alpine:latest

LABEL author="Konrad Baechler <konrad@diva.exchange>, Moritz Küttel <moritz.kuettel@gmail.com>" \
  maintainer="Moritz Küttel <moritz.kuettel@gmail.com>"

COPY entrypoint.sh /home/i2preseed/
COPY src /home/i2preseed/src

RUN apk --no-cache --virtual build-dependendencies add \
    binutils \
    go \
  && export GOPATH=/home/i2preseed/ \
  && cd /home/i2preseed/src/i2p-tools \
  && go install \
  && strip /home/i2preseed/bin/i2p-tools \
  && cd /home/i2preseed/ \
  # remove build dependencies
  && rm -rf src \
  && rm -rf pkg \
  && apk --no-cache --purge del build-dependendencies \
  # set permissions
  && chmod 0700 bin/i2p-tools \
  && chmod +x entrypoint.sh \
  && mkdir netDb


# 8443 reseed server
EXPOSE 8443

VOLUME [ "/home/i2preseed/netDb" ]
WORKDIR "/home/i2preseed"
ENTRYPOINT [ "/home/i2preseed/entrypoint.sh" ]

