#!/bin/sh
#
# Author: konrad@diva.exchange
# Author/Maintainer: moritz.kuettel@gmail.com
#

# -e  Exit immediately if a simple command exits with a non-zero status
set -e

IP_CONTAINER=`ip route get 1 | awk '{ print $NF; exit; }'`

# mandatory signer id, like something@somedomain.tld
SIGNER=${SIGNER:?Pass signer ID, like something@somedomain.tld}

# create the keys, if they don't exist
[ -f /home/i2preseed/${SIGNER}.lock ] || \
    /home/i2preseed/bin/i2p-tools keygen --signer=${SIGNER} --tlsHost=reseed
touch /home/i2preseed/${SIGNER}.lock

netdb_dir=/home/i2preseed/netDb

if [ 1 -eq "${WAIT_ROUTER_INFOS_COLLECTED:-0}" ]; then
    while [ ! -f "$netdb_dir"/.router-infos-collected ]; do
        echo "Waiting for initial router infos, have $(printf "%s\n" "$netdb_dir"/routerInfo*.dat | wc -l)"
        sleep 1
    done
fi


/home/i2preseed/bin/i2p-tools reseed \
  --signer=${SIGNER} \
  --tlsHost=reseed \
  --netdb="$netdb_dir" \
  --port=8443 \
  --ip=${IP_CONTAINER} \
  --numRi="${NUM_ROUTER_INFOS:-77}"
  --numSu3="${NUM_SU3_FILES:-0}"
  --trustProxy
