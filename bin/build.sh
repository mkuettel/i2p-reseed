#!/usr/bin/env bash
#
# Maintainer: konrad@diva.exchange
# Maintainer: moritz.kuettel@gmail.com

set -e

PROJECT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${PROJECT_PATH}/../

# docker image
docker build --no-cache -t mkuettel/i2p-reseed:latest .
